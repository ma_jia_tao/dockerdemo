package com.itcast.dockerdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author majiatao
 * @Date 19-11-13
 */

@RestController
public class IndexController {
    @GetMapping("index")
    public String index() {
        return "<a href='www.baidu.com'>baidu </a>";
    }
}
