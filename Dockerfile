FROM java:8
MAINTAINER majiatao
ADD dockerdemo-0.0.3.jar app.jar
RUN bash -c 'touch /app.jar'
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar"]
